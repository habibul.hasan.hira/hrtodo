import React, { Component } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class Signup extends Component {
  constructor(props){
    super(props);
     this.state = {
      name:'',
      last_name:'',
      email : '',
      password:null
    }
  }
  changeView = ()=>{
   this.props.handleViewChange();
  }

  notifyOnSuccess = (message) => toast.success(message);
  notifyOnFailure = (message) => toast.error(message);

   validateEmail=(email)=> {
    var re = /^[A-Za-z0-9._%+-]+@misfit.tech/;
    return re.test(String(email).toLowerCase());
   }
  handleSubmit = (event) => {
    event.preventDefault();

    let signUpInformation = {
      name:event.target.name.value,
      last_name: event.target.last_name.value,
      email: event.target.email.value,
      type: event.target.type.value,
      password: event.target.password.value,

    }
    if(!this.validateEmail(signUpInformation.email)){ 
      this.notifyOnFailure('Please use valid email address')
       return
    }
    if(!signUpInformation.type) {
      this.notifyOnFailure('Please check a type')
       return
    }
    
    axios.post("http://localhost:5000/users", signUpInformation)
    .then(data => {
      this.changeView();
    })
    .catch((error)=>{
      console.log(error);
    });  
  }
  

  render() {
    if(!this.props.isSignUpActive) return null;
    else {
      return (
        <div>
        <div className="container">
          <br />  <p className="text-center">Create Account</p>
          <hr />
          <div className="row justify-content-center">
            <div className="col-md-6">
              <div className="card">
                <header className="card-header">
                  <a onClick = {this.changeView} href className="float-right btn btn-outline-primary mt-1">Log in</a>
                  <h4 className="card-title mt-2">Sign up</h4>
                </header>
                <article className="card-body">
                  <form onSubmit={this.handleSubmit}>
                    <div className="form-row">
                      <div className="col form-group">
                        <label>First name </label>   
                        <input type="text" id="name" required className="form-control"  />
                      </div> 
                      <div className="col form-group">
                        <label>Last name</label>
                        <input type="text" name = "last_name" required className="form-control" />
                      </div> {/* form-group end.// */}
                    </div> {/* form-row end.// */}
                    <div className="form-group">
                      <label>Email address</label>
                      <input type="email" name="email" required className="form-control"  />
                      <small className="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div> {/* form-group end.// */}
                    <div className="form-group">
                      <label className="form-check form-check-inline">
                        <input className="form-check-input" type="radio"  name="type" defaultValue="HR" />
                        <span className="form-check-label"> HR </span>
                      </label>
                      <label className="form-check form-check-inline">
                        <input className="form-check-input" type="radio" name="type" defaultValue="Employee" />
                        <span className="form-check-label"> Employee</span>
                      </label>
                      <label className="form-check form-check-inline">
                        <input className="form-check-input" type="radio" name="type" defaultValue="Owner" />
                        <span className="form-check-label"> Manager</span>
                      </label>
                    </div> {/* form-group end.// */}
                    <div className="form-group">
                      <label>Create password</label>
                      <input className="form-control" required name="password" type="password" />
                    </div> {/* form-group end.// */}  
                    <div className="form-group">
                      <button type="submit" className="btn btn-primary btn-block"> Register</button>
                    </div> {/* form-group// */}      
                    <small className="text-muted">By clicking the 'Sign Up' button, you confirm that you accept our <br /> Terms of use and Privacy Policy.</small>                                          
                  </form>
                </article> {/* card-body end .// */}
                <div className="border-top card-body text-center">Have an account? <a href>Log In</a></div>
              </div> {/* card.// */}
            </div> {/* col.//*/}
          </div> {/* row.//*/}
        </div> 
        {/*container end.//*/}
        <br /><br />
        <ToastContainer position="top-right"
                    autoClose={1500}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnVisibilityChange
                    draggable
                    pauseOnHover 
          />
        <article className="bg-secondary mb-3">  
          <div className="card-body text-center">
            <h3 className="text-white mt-3">HR Task Manager Tool</h3>
            <p className="h5 text-white">You can add and monitor   <br /> all tasks of your company  
              invite  your employee to use it </p>   <br />
            <p><a className="btn btn-warning"  href="http://bootstrap-ecommerce.com/"> made for monitoring request
                <i className="fa fa-window-restore " /></a></p>
          </div>
          <br /><br />
        </article>
      </div>
      );
    }
    }
    
}

export default Signup;
