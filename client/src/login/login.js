import React, { Component } from 'react';
import axios from 'axios';
class Login extends Component {
  constructor(props){
    super(props);
     this.state = {
      email : '',
      password:null
    }
  }
  
  takeUserToTaskEntry = (data)=>{
    localStorage.setItem('userType',data.user.type);
    this.props.changeViewToTaskEntry(data)
  }
  handleSubmit = (event) => {
    event.preventDefault();

    let  logInformation = {
      email: event.target.email.value,
      password: event.target.password.value,
    }
    
    axios.post("http://localhost:5000/users/login", logInformation)
    .then(data => {
      this.takeUserToTaskEntry(data.data);
    });  
  }
  changeView = ()=>{
   this.props.handleViewChange();
  }
  render() {
    if(!this.props.isLoginActive) return null;
    else {
    return (
      <div>
      <div className="container">
        <br />  <p className="text-center">Login to your account</p>
        <hr />
        <div className="row justify-content-center">
          <div className="col-md-6">
            <div className="card">
              <header className="card-header">
                <a onClick = {this.changeView} href className="float-right btn btn-outline-primary mt-1">Sign up?</a>
                <h4 className="card-title mt-2">Login</h4>
              </header>
              <article className="card-body">
                <form onSubmit={this.handleSubmit}>
                  <div className="form-group">
                    <label>Email</label>
                    <input type="email" name="email" className="form-control"  />
                    <small className="form-text text-muted">We'll never share your email with anyone else.</small>
                  </div> {/* form-group end.// */}
                 
                  <div className="form-group">
                    <label> password</label>
                    <input className="form-control" name="password" type="password" />
                  </div> {/* form-group end.// */}  
                  <div className="form-group">
                    <button type="submit" className="btn btn-primary btn-block">Login</button>
                  </div> {/* form-group// */}      
                  
                </form>
              </article> {/* card-body end .// */}
            </div> {/* card.// */}
          </div> {/* col.//*/}
        </div> {/* row.//*/}
      </div> 
      {/*container end.//*/}
      <br /><br />
      <article className="bg-secondary mb-3">  
          <div className="card-body text-center">
            <h3 className="text-white mt-3">HR Task Manager Tool</h3>
            <p className="h5 text-white">You can add and monitor   <br /> all tasks of your company  
              invite  your employee to use it </p>   <br />
            <p><a className="btn btn-warning"  href="http://bootstrap-ecommerce.com/"> made for monitoring request
                <i className="fa fa-window-restore " /></a></p>
          </div>
          <br /><br />
        </article>
    </div>
    );
    }
  }
}

export default Login;
