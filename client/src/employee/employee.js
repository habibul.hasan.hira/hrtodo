import React, { Component } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class Employee extends Component {
  constructor(props){
    super(props);
     this.state = {
      email : '',
      password:null,
      rows:null,
      description:'',
      editMode : false
    }
  }

takeUserToTaskEntry = (data)=>{
    this.props.changeViewToTaskEntry(data)
  }

handleRowClick =(event)=>{
    event.preventDefault();
    let rowInfo = event.currentTarget.attributes['row'].value;
    axios.delete('http://localhost:5000/tasks',{
      params: {
        id: JSON.parse(rowInfo)._id
      }
    })
    .then(({data}) => {
      this.notifyOnSuccess('Task Deleted');
      setTimeout(()=>{
        window.location.reload();
      },500)
     
    })
    .catch((error)=>{
      this.notifyOnFailure('Can not delete Task')
    });
}
handleRowEdit =(event)=>{
  event.preventDefault();
  let rowInfo = event.currentTarget.attributes['row'].value;
  this.setState({description:JSON.parse(rowInfo).description});
  this.setState({editMode:true,selectedRowId:JSON.parse(rowInfo)._id});
}
componentDidMount() {
    let id = localStorage.getItem('userId');
    axios.get('http://localhost:5000/tasksList',{
      params : {
        id:id
      }
    })
    .then(({data}) => {
      const tableRows = data.map((task) => {
        return (<tr key = {task._id}>
          <td>{task.description}</td>
          <td>{task.complete || 'not completed yet'}</td>
          <td className=" btn-link" row = {JSON.stringify(task)} onClick={this.handleRowClick}>Delete</td>
          <td className=" btn-link" row = {JSON.stringify(task)} onClick={this.handleRowEdit}>Edit</td>
        </tr>)
      });
      this.setState({rows:tableRows});
      
    })
    .catch((error)=>{
      console.log(error);
      
    });  
  }
handleDescriptionChange = (event)=>{
  event.preventDefault();
  let description = event.currentTarget.value;
  this.setState({description});
  console.log(description);
}

handleSubmit = (event) => {

    event.preventDefault();

    let taskInformation = {
      description: event.target.description.value,
      _id:this.props._uid
    }

    if(this.state.editMode === false){
      axios.post("http://localhost:5000/tasks", taskInformation)
      .then(data => {
        this.notifyOnSuccess("Task created");
        this.setState({description:''});
        setTimeout(() => {
          window.location.reload();
        }, 1300);
      })
      .catch((error)=>{
        this.notifyOnFailure('Can not create task');
      });  

    } else {
      let info = {
        description: this.state.description,
        owner:this.props._uid,
        id:this.state.selectedRowId,
        completed:false
      }
        axios.put('http://localhost:5000/tasks',info)
        .then(({data}) => {
            this.notifyOnSuccess('Task updated');
            setTimeout(()=>{
              window.location.reload();
            },1000)
        })
        .catch((error)=>{
          this.notifyOnFailure('Can not update Task')
        });

        this.setState({editMode:false});
    }
    
  }
  notifyOnSuccess = (message) => toast.success(message);
  notifyOnFailure = (message) => toast.error(message);
  render() {
    if (!this.props.shouldVisible) return null;
    else {
    return (
      <div>
      <div className="container">
        <br />  <p className="text-center">Create a Request to HR</p>
        <hr />
        <div className="row justify-content-center">
          <div className="col-md-6">
            <div className="card">
              <article className="card-body">
                <form onSubmit={this.handleSubmit}>
                  <div className="form-group">
                    <label>Task description</label>
                    <input type="text" name="description" onChange={this.handleDescriptionChange} value={this.state.description} className="form-control"  />
                    <small className="form-text text-muted">Place your request to HR</small>
                  </div>                  
                
                  <div className="form-group">
                    <button type="submit" className="btn btn-primary btn-block">Save</button>
                  </div>   
                  
                </form>
              </article> 
              <br></br>
            </div> 
          </div> 
        </div> 
      </div> 
      <br /><br />
      <ToastContainer position="top-right"
                    autoClose={1200}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnVisibilityChange
                    draggable
                    pauseOnHover 
          />
      <div className="row justify-content-center">
          <div className="col-md-6">
           
      <div class="table-responsive">     
      <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col">Description</th>
              <th scope="col">Completed</th>
              <th scope="col">Delete</th>
              <th scope="col">Edit</th>
            </tr>
          </thead>
          <tbody>
            {this.state.rows}
          </tbody>
      </table>
      </div>
          </div>
          </div>
    </div>
    );
    }
  }
}

export default Employee;
