import React, { Component } from 'react';
import './App.css';
import Signup from './signup/signup';
import Login from './login/login';
import Employee from './employee/employee';
import HrDashboard from './hr/HrDashboard';
class App extends Component {

  constructor (props) {
    super(props);
    this.state = {
     isLoginActive:false,
     isSignUpActive: (localStorage.getItem( 'isActive' )) === 'true'? false: true,
     loggedIn:(localStorage.getItem( 'isActive' )) === 'true'? true: false,
     employeeWindow: (localStorage.getItem( 'userInfo' )) === 'true'? true: false,
     loggedInUserId:localStorage.getItem( 'userId' ) || null,
     hrDashboard: (localStorage.getItem( 'userInfo' )) === 'hrview'? true: false,
     userType: localStorage.getItem( 'userType' ) || null,
    }
  }  
  handleViewChange = ()=>{
    this.setState({
      isLoginActive : !(this.state.isLoginActive),
      isSignUpActive: !(this.state.isSignUpActive)
    })
  }

  logout = ()=>{
   localStorage.clear();
    this.setState({
      isLoginActive : false,
      isSignUpActive: true
    })
    window.location.reload();
  }
  changeViewToTaskEntry = (data)=> {
    localStorage.setItem('userType','sada');
    if(data.user.type ==='Employee') {
      localStorage.setItem('isActive','true' )
      localStorage.setItem('userInfo',true);
      this.setState({
        employeeWindow : true,
        isLoginActive : false,
        loggedInUserId:data.user._id,
        loggedIn:true
      })
    } else if(data.user.type ==='HR'  || data.user.type ==='Owner' ) {
      localStorage.setItem( 'userType',data.user.type)
      localStorage.setItem('userInfo','hrview');
      localStorage.setItem('isActive','true');
      this.setState({
        isLoginActive : false,
        loggedInUserId:data.user._id,
        loggedIn:true,
        hrDashboard: true
      })
    }
    localStorage.setItem('userId',data.user._id);
    window.location.reload();
    
  }
  render() {
    console.log('called');
    
    return ( 
      <div className="App">
       <nav className="navbar navbar-expand-lg navbar-light bg-primary">
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
              <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div className="navbar-nav">
          <button onClick = {this.logout} type="button" className="btn btn-info">Logout</button>
          </div>
        </div>
      </nav>
      <Signup isSignUpActive = {this.state.isSignUpActive} 
      handleViewChange={this.handleViewChange} />

      <Login 
        isLoginActive= {this.state.isLoginActive} 
        handleViewChange={this.handleViewChange}
        changeViewToTaskEntry = {this.changeViewToTaskEntry}
      />
  
      <Employee  
        shouldVisible = {this.state.employeeWindow} 
        _uid = {this.state.loggedInUserId} 
      />

      <HrDashboard  
          shouldVisible = {this.state.hrDashboard} 
          _uid = {this.state.loggedInUserId} 
          userType = {localStorage.getItem( 'userType' )}
      />
      </div>
    );
  }
}

export default App;
