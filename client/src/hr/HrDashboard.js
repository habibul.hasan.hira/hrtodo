import React, { Component } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ReactSwitch from './ReactSwitch'

class HrDashboard extends Component {

  constructor(props){
    super(props);
     this.state = {
      email : '',
      password:null,
      rows:null,
      description:'',
      editMode : false,
      type: localStorage.getItem( 'userType' ) || null
    }
  }
handleRowEdit =(event)=>{
  event.preventDefault();
  let rowInfo = event.currentTarget.attributes['row'].value;
  this.setState({description:JSON.parse(rowInfo).description});
  this.setState({editMode:true,selectedRowId:JSON.parse(rowInfo)._id});
}
  
componentDidMount() {
    axios.get('http://localhost:5000/tasks')
    .then(({data}) => {
      if(this.state.type === 'HR' || localStorage.getItem( 'userType' )==='HR')
      {
        const tableRows = data.map((task) => {
          return (<tr key = {task._id}>
            <td>{task.description}</td>
            <td row = {JSON.stringify(task)}><ReactSwitch  checked= {task.open}  rowName={'open'} rowData = {JSON.stringify(task)}/></td>
            <td row = {JSON.stringify(task)}><ReactSwitch checked= {task.reviewed} rowName={'reviewed'} rowData = {JSON.stringify(task)}/></td>
            <td row = {JSON.stringify(task)}><ReactSwitch checked= {task.processed} rowName={'processed'} rowData = {JSON.stringify(task)}/></td>
          
          </tr>)
        });
        this.setState({rows:tableRows});
      } else if (this.state.type === 'Owner') {
        const tableRows = data.map((task) => {
          return (<tr key = {task._id}>
            <td>{task.description}</td>
            <td row = {JSON.stringify(task)}><ReactSwitch  checked= {task.open}  rowName={'open'} rowData = {JSON.stringify(task)}/></td>
            <td>{task.reviewed?'Reviewed':'Not reviewed yet'}</td>
            <td disable row = {JSON.stringify(task)}> {task.reviewed?<ReactSwitch checked= {task.processed} rowName={'processed'} rowData = {JSON.stringify(task)}/>:'waiting for review'} </td>
          </tr>)
        });
        this.setState({rows:tableRows}); 
      } 

    })
    .catch((error)=>{
      console.log(error);
      
    });  
  }

  notify = (message) => toast(message);
  render() {
    if (!this.props.shouldVisible) return null;
    else {
    return (
      <div>
      <div className="container">
        <br />  <p className="text-center"> HR Dashboard</p>
        <hr />
        
      </div> 
      <br /><br />
      <ToastContainer position="top-right"
                    autoClose={1200}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnVisibilityChange
                    draggable
                    pauseOnHover 
          />
      <div className="row justify-content-center">
          <div className="col-md-6">
           
      <div className="table-responsive">     
      <table className="table table-bordered">
          <thead>
            <tr>
              <th scope="col">Description</th>
              <th scope="col">Open</th>
              <th scope="col">Reviewed</th>
              <th scope="col">Processed</th>
            </tr>
          </thead>
          <tbody>
            {this.state.rows}
          </tbody>
      </table>
      </div>
          </div>
          </div>
    </div>
    );
    }
  }
}

export default HrDashboard;
