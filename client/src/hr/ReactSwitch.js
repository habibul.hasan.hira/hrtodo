import React, { Component } from "react";
import Switch from "react-switch";
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
 
export default class ReactSwitch extends Component {
  constructor(props) {
    super(props);
    this.state = { checked: this.props.checked || false };
    this.handleChange = this.handleChange.bind(this);
  }
 
  notifyOnSuccess = (message) => toast.success(message);
  notifyOnFailure = (message) => toast.error(message);

  handleChange(checked) {
    console.log(this.props.rowData);
    let rowInfo = JSON.parse(this.props.rowData);
    if(this.props.rowName ==='open'){

      rowInfo.open = this.props.checked? false : true;
    }
    if(this.props.rowName ==='processed') {
    rowInfo.processed = this.props.checked? false : true;
    }
    if(this.props.rowName ==='reviewed') {
      rowInfo.reviewed = this.props.checked? false : true;
      }
    axios.put('http://localhost:5000/hractions',rowInfo)
        .then(({data}) => {
          this.setState({ checked });
          this.notifyOnSuccess('Task updated');
        })
        .catch((error)=>{
          this.setState({ checked });
          this.notifyOnFailure('Can not update Task')
        });

    
  }
 
  render() {
    return (
      <label>
        <Switch onChange={this.handleChange} checked={this.state.checked} />
        <ToastContainer position="top-right"
                    autoClose={1200}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnVisibilityChange
                    draggable
                    pauseOnHover 
          />
      </label>
      
    );
  }
}