# Project Title

HR task manager application built with 

*  NODE
*  EXRESS
*  REACT
*  MONGODB
*  Mongoose
*  ES6

## Getting Started

This document will give specific information about how to run this project in a given environemnt. This project has two part 
one is backend another is client application in react. Backend and front end. 
### Prerequisites

Before installing the software please do check the below requirements 

```
node LTS
```

```
mongodb
```

```
React LTS
```


### Installing



Installing npm modules for server, go to app folder and open terminal on root folder and run 

```
npm install --ver 
```

Installing npm modules for client , go to client folder and open terminal on root folder and run

```
npm install --ver
```

To run the entire application concurrently used, to run concurrently, go to app folder and run 

## npm run dev

API end points 

### Break down into end to end tests


To create user , send data with body by setting POST as request type
```
SET TYPE: POST [in postman]
http://localhost:5000/users
```

To login user, send data in request body 
```
SET TYPE: POST [in postman]
http://localhost:5000/users/login
```

To get all tasks of user 
```
SET TYPE: GET [in postman]
http://localhost:5000/tasksList?id={ownerid}
```

To get all tasks of Manager and HR  
```
SET TYPE: GET [in postman]
http://localhost:5000/tasks
```

To create task, give owner id and task descrition in  body.
```
SET TYPE: POST [in postman]
http://localhost:5000/tasks
```

To update task, give owner id and task descrition in  body.
```
SET TYPE: PUT [in postman]
http://localhost:5000/tasks
```


## Built With

* [NODE](Server side ) 
* [Express](web framework) 
* [Recct](front end tool) 
* [mongodb](database)
* [mongoose](ORM)


## Versioning

Used git as a versioning tool.

## Authors

* **Habibul Hasan** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Stask overflow
* Steve
* Udemy
