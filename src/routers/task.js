const express = require('express')
const Task = require('../models/task')
const auth = require('../middleware/auth')
const user = require('../models/user');
const router = new express.Router()

/** 
 * to create a task request
 * @param req
 * @param res
*/
router.post('/tasks', async (req, res) => {
    const task = new Task({
        description: req.body.description,
        owner: req.body._id
    })

    try {
        await task.save()
        res.status(201).send(task);
      
        
    } catch (e) {
        res.status(400).send(e)
    }
})


/** 
 * to get all tasks of a owner
 * @param req
 * @param res
*/
router.get('/tasksList', async (req, res) => {
    const _id = req.query.id

    try {
        const task = await Task.find({owner: _id})

        if (!task) {
            return res.status(404).send()
        }

        res.send(task)
    } catch (e) {
        res.status(500).send()
    }
})
/** 
 * to get all tasks for hr and manager view
 * @param req
 * @param res
*/
router.get('/tasks', async (req, res) => {
    const _id = req.params.id;

    try {
        const OwnersTasks = await Task.find();
        res.send(OwnersTasks);
    } catch (e) {
        res.status(500).send()
    }
})

/** 
 * to update a task
 * @param req
 * @param res
*/
router.put('/tasks', async (req, res) => {
    const updates = Object.keys(req.body)
    try {
        const task = await Task.findOne({ _id: req.body.id, owner: req.body.owner})

        if (!task) {
            return res.status(404).send()
        }
        updates.forEach((update) => task[update] = req.body[update])
        await task.save()
        res.send(task)
    } catch (e) {
        res.status(400).send(e)
    }
})


/** 
 * to update a task by hr
 * @param req
 * @param res
*/
router.put('/hractions', async (req, res) => {
    const updates = Object.keys(req.body)
    try {
        const task = await Task.findOne({ _id: req.body._id})

        if (!task) {
            return res.status(404).send()
        }
        updates.forEach((update) => task[update] = req.body[update])
        await task.save()
        res.send(task)
    } catch (e) {
        res.status(400).send(e)
    }
})

/** 
 * to delete a task 
 * @param req
 * @param res
*/
router.delete('/tasks', async (req, res) => {
    console.log(req.query.id);
    
    try {
        const task = await Task.findOneAndDelete({ _id: req.query.id})
        if (!task) {
            res.status(404).send()
        }
        res.send(task)
    } catch (e) {
        res.status(500).send()
    }
})

module.exports = router